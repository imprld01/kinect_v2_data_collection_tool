#pragma once

// C
#include <Kinect.h>
#include <Windows.h>
#include <opencv2/opencv.hpp>

// C++
#include <sstream>
#include <iomanip>
#include <fstream>
#include <iostream>

// self-define lib
#include "Kinectv2.h"
#include "targetver.h"
#include "KinectDatasetFileManager.h"

using namespace cv;
using namespace std;

// self-define function in stdafx.cpp
void DrawBone(Mat&, CvPoint [], const Joint*, int, JointType, JointType);
void drawSkeleton(Mat&, CvPoint [], const Joint*, int);