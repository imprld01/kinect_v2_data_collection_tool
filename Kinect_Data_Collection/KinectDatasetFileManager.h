#pragma once

#include <string>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

class KinectDatasetFileManager {

private:
	int sampleID = 0;

	ofstream oFile;
	string parentPath;
	string parentDirName;
	string colorDirName;
	string depthDirName;
	string depthMatDirName;
	string bodyDirName;
	string bodyFileName;

	string joints[25] = { "Base_Spine", "Middle_Spine", "Neck", "Head", "Right_Shoulder",
						  "Right_Elbow", "Right_Wrist", "Right_Hand", "Left_Shoulder", "Left_Elbow",
						  "Left_Wrist", "Left_Hand", "Right_Hip", "Right_Knee", "Right_Ankle",
						  "Right_Foot", "Left_Hip", "Left_Knee", "Left_Ankle", "Left_Foot",
						  "Shoulder_Spine", "Right_Hand_Tip", "Right_Thumb", "Left_Hand_Tip", "Left_Thumb" };

public:
	KinectDatasetFileManager(string, string, string, string, string, string, string);
	~KinectDatasetFileManager();

	int getSampleID();
	int getCurrentSampleID();
	string getActivityName();
	string getDateTimeStrg();
	string getColorDirPath();
	string getDepthDirPath();
	string getDepthMatDirPath();
	string getBodyDirPath();
	string getBodyFilePath();

	Mat readImage(string, string);

	void writeCSV(string, string, string, string);
	void writeCSV(string, string, int, int);
	void writeCSV(string, string);
	void writeCSV(int, int);
	void writeMatCSV(string, Mat);
	void writeEndl();
	void writeImage(string, string, Mat);
};