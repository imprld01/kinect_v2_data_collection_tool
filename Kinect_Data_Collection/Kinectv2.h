#pragma once

#include <Kinect.h>
#include <opencv2/opencv.hpp>

#include "DataBean.h"
#include "KinectDatasetFileManager.h"

using namespace cv;
using namespace std;

class KinectV2 {

private:
	int colorWidth = 1920;
	int colorHeight = 1080;

	int depthWidth = 512;
	int depthHeight = 424;

	unsigned short minDetectDepth = 500;
	unsigned short maxDetectDepth = 4500;

	HRESULT hResult = S_OK;
	IKinectSensor* pSensor;

	ICoordinateMapper* pCoordinateMapper;

	IBodyFrameSource* pBodySource;
	IBodyFrameReader* pBodyReader;

	IColorFrameSource* pColorSource;
	IColorFrameReader* pColorReader;
	IFrameDescription* pColorDescription;

	IDepthFrameSource* pDepthSource;
	IDepthFrameReader* pDepthReader;
	IFrameDescription* pDepthDescription;

	bool updateSuccess = false;

	Mat colorBufferMat;
	vector<Mat> colorMatVector;
	unsigned int colorBufferSize;

	IBody* pBodyBuffer[BODY_COUNT] = { 0 };
	struct AllBodyBean bodyInfo;
	vector<struct AllBodyBean> bodyInfoVector;

	Mat depthBufferMat;
	Mat depthBufferMatClone;
	vector<Mat> depthMatVector;
	vector<DepthSpacePoint> depthSpacePoints{ 1920 * 1080 };
	vector<vector<DepthSpacePoint>> depthMapPointsVector;
	unsigned int depthBufferSize;

	KinectDatasetFileManager* kdfm;

	void initKinect();
	void setupBuffer();
	bool getSensorManager();
	bool getCoordinateManager();
	bool setupColorSensor();
	bool setupDepthSensor();
	bool setupBodySensor();
	void createColorMat();
	void createDepthMat();
	void getDepthDetectRange();
	void readColorMat();
	void readDepthMat();
	void readBodyData();
	void extractBodyData();
	void appendData();
	Mat mappingDepthToColorSize(Mat, vector<DepthSpacePoint>);

public:
	KinectV2(KinectDatasetFileManager *);
	~KinectV2();
	void update();
	void save();
};