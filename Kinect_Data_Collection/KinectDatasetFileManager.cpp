#include "stdafx.h"
#include "KinectDatasetFileManager.h"

using namespace cv;
using namespace std;

KinectDatasetFileManager::KinectDatasetFileManager(string parentPath, string parentDirName, string colorDirName,
												   string depthDirName, string depthMatDirName, string bodyDirName, string bodyFileName) {

	this->parentPath = parentPath;
	this->parentDirName = parentDirName;
	this->colorDirName = colorDirName;
	this->depthDirName = depthDirName;
	this->depthMatDirName = depthMatDirName;
	this->bodyDirName = bodyDirName;
	this->bodyFileName = bodyFileName;

	CreateDirectoryA(this->parentPath.c_str(), NULL);
	CreateDirectoryA((this->parentPath + "/" + this->parentDirName).c_str(), NULL);
	CreateDirectoryA(getColorDirPath().c_str(), NULL);
	CreateDirectoryA(getDepthDirPath().c_str(), NULL);
	CreateDirectoryA(getDepthMatDirPath().c_str(), NULL);
	CreateDirectoryA(getBodyDirPath().c_str(), NULL);

	oFile.open(getBodyFilePath(), ios::out | ios::trunc);

	// write header in csv (first line)
	writeCSV("frame", "timestamp", "width", "height");
	for (int i = 0; i < JointType::JointType_Count; ++i) {
		string joint = joints[i];
		writeCSV(joint + "_x", joint + "_y");
	}
	writeEndl();
}

KinectDatasetFileManager::~KinectDatasetFileManager() { oFile.close(); }

int KinectDatasetFileManager::getSampleID() { return sampleID; }

int KinectDatasetFileManager::getCurrentSampleID() { return sampleID++; }

string KinectDatasetFileManager::getActivityName() { return parentDirName; }

string KinectDatasetFileManager::getDateTimeStrg() {

	time_t rawtime;
	char buffer[80];
	struct tm * timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buffer, 80, "%d-%m-%Y_%H-%M-%S", timeinfo);
	
	return string(buffer);
}

string KinectDatasetFileManager::getColorDirPath() { return parentPath + "/" + parentDirName + "/" + colorDirName; }

string KinectDatasetFileManager::getDepthDirPath() { return parentPath + "/" + parentDirName + "/" + depthDirName; }

string KinectDatasetFileManager::getDepthMatDirPath() { return parentPath + "/" + parentDirName + "/" + depthMatDirName; }

string KinectDatasetFileManager::getBodyDirPath() { return parentPath + "/" + parentDirName + "/" + bodyDirName; }

string KinectDatasetFileManager::getBodyFilePath() { return parentPath + "/" + parentDirName + "/" + bodyFileName; }

Mat KinectDatasetFileManager::readImage(string dirPath, string fileName) { return imread(dirPath + "/" + fileName); }

void KinectDatasetFileManager::writeCSV(string str, string timestamp, string width, string height) { oFile << str << "," << timestamp << "," << width << "," << height; }

void KinectDatasetFileManager::writeCSV(string str, string time, int width, int height) { oFile << str << "," << time << "," << width << "," << height; }

void KinectDatasetFileManager::writeCSV(string x, string y) { oFile << "," << x << "," << y; }

void KinectDatasetFileManager::writeCSV(int x, int y) { oFile << "," << x << "," << y; }

void KinectDatasetFileManager::writeMatCSV(string fileName, Mat depthMat) {

	ofstream myfile;
	string path = this->getDepthMatDirPath() + "/" + fileName;
	myfile.open(path.c_str());

	myfile << cv::format(depthMat, cv::Formatter::FMT_CSV) << std::endl;

	myfile.close();
}

void KinectDatasetFileManager::writeEndl() { oFile << endl; }

void KinectDatasetFileManager::writeImage(string dirPath, string fileName, Mat img) { imwrite(dirPath + "/" + fileName, img); }