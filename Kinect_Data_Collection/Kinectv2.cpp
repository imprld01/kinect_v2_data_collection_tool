#include "stdafx.h"
#include "Kinectv2.h"

using namespace cv;
using namespace std;

template<class Interface>
inline void SafeRelease(Interface *& pInterfaceToRelease) {
	if (pInterfaceToRelease != NULL) {
		pInterfaceToRelease->Release();
		pInterfaceToRelease = NULL;
	}
}

void KinectV2::initKinect() {

	getSensorManager();
	getCoordinateManager();

	setupBodySensor();
	setupColorSensor();
	setupDepthSensor();
}

void KinectV2::setupBuffer() {

	createColorMat();
	createDepthMat();

	getDepthDetectRange();
}

bool KinectV2::getSensorManager() {
		
	hResult = GetDefaultKinectSensor(&pSensor);
	if (FAILED(hResult)) {
		std::cerr << "Error : GetDefaultKinectSensor" << std::endl;
		return false;
	}
	hResult = pSensor->Open();
	if (FAILED(hResult)) {
		std::cerr << "Error : IKinectSensor::Open()" << std::endl;
		return false;
	}

	return true;
}

bool KinectV2::getCoordinateManager() {

	hResult = pSensor->get_CoordinateMapper(&pCoordinateMapper);
	if (FAILED(hResult)) {
		std::cerr << "Error : IKinectSensor::get_CoordinateMapper()" << std::endl;
		return false;
	}

	return true;
}

bool KinectV2::setupColorSensor() {

	hResult = pSensor->get_ColorFrameSource(&pColorSource);
	if (FAILED(hResult)) {
		std::cerr << "Error : IKinectSensor::get_ColorFrameSource()" << std::endl;
		return false;
	}

	hResult = pColorSource->OpenReader(&pColorReader);
	if (FAILED(hResult)) {
		std::cerr << "Error : IColorFrameSource::OpenReader()" << std::endl;
		return false;
	}

	hResult = pColorSource->get_FrameDescription(&pColorDescription);
	if (FAILED(hResult)) {
		std::cerr << "Error : IColorFrameSource::get_FrameDescription()" << std::endl;
		return false;
	}

	return true;
}

bool KinectV2::setupDepthSensor() {

	hResult = pSensor->get_DepthFrameSource(&pDepthSource);
	if (FAILED(hResult)) {
		std::cerr << "Error : IKinectSensor::get_DepthFrameSource()" << std::endl;
		return false;
	}

	hResult = pDepthSource->OpenReader(&pDepthReader);
	if (FAILED(hResult)) {
		std::cerr << "Error : IDepthFrameSource::OpenReader()" << std::endl;
		return false;
	}

	hResult = pDepthSource->get_FrameDescription(&pDepthDescription);
	if (FAILED(hResult)) {
		std::cerr << "Error : IDepthFrameSource::get_FrameDescription()" << std::endl;
		return false;
	}

	return true;
}

bool KinectV2::setupBodySensor() {

	hResult = pSensor->get_BodyFrameSource(&pBodySource);
	if (FAILED(hResult)) {
		std::cerr << "Error : IKinectSensor::get_BodyFrameSource()" << std::endl;
		return false;
	}

	hResult = pBodySource->OpenReader(&pBodyReader);
	if (FAILED(hResult)) {
		std::cerr << "Error : IBodyFrameSource::OpenReader()" << std::endl;
		return false;
	}

	return true;
}

void KinectV2::createColorMat() {

	pColorDescription->get_Width(&colorWidth); // 1920
	pColorDescription->get_Height(&colorHeight); // 1080

	colorBufferSize = colorWidth * colorHeight * 4 * sizeof(unsigned char);
	colorBufferMat.create(colorHeight, colorWidth, CV_8UC4);
}

void KinectV2::createDepthMat() {

	pDepthDescription->get_Width(&depthWidth); // 512
	pDepthDescription->get_Height(&depthHeight); // 424
	depthBufferSize = depthWidth * depthHeight * sizeof(unsigned short);
	depthBufferMat.create(depthHeight, depthWidth, CV_16UC1);
}

void KinectV2::getDepthDetectRange() {

	// Range (Range of Depth is 500-8000[mm], Range of Detection is 500-4500[mm])
	pDepthSource->get_DepthMinReliableDistance(&minDetectDepth); // 500
	pDepthSource->get_DepthMaxReliableDistance(&maxDetectDepth); // 4500
}

void KinectV2::readColorMat() {

	IColorFrame* pColorFrame = nullptr;
	HRESULT hResult = pColorReader->AcquireLatestFrame(&pColorFrame);
	if (SUCCEEDED(hResult)) {
		hResult = pColorFrame->CopyConvertedFrameDataToArray(colorBufferSize, reinterpret_cast<BYTE*>(colorBufferMat.data), ColorImageFormat_Bgra);
		if (!SUCCEEDED(hResult)) updateSuccess = false;
	}
	else updateSuccess = false;
	SafeRelease(pColorFrame);
}

void KinectV2::readDepthMat() {

	IDepthFrame* pDepthFrame = nullptr;
	hResult = pDepthReader->AcquireLatestFrame(&pDepthFrame);
	if (SUCCEEDED(hResult)) {
		hResult = pDepthFrame->AccessUnderlyingBuffer(&depthBufferSize, reinterpret_cast<UINT16**>(&depthBufferMat.data));
		if (SUCCEEDED(hResult)) {
			depthBufferMatClone = depthBufferMat.clone();
			//vector<DepthSpacePoint> depthSpacePoints(colorHeight*colorWidth);
			hResult = pCoordinateMapper->MapColorFrameToDepthSpace(depthBufferSize, (UINT16*)depthBufferMat.data, colorHeight*colorWidth, &depthSpacePoints[0]);
			if (!SUCCEEDED(hResult)) updateSuccess = false;
		}
		else updateSuccess = false;
	}
	else updateSuccess = false;
	SafeRelease(pDepthFrame);
}

void KinectV2::readBodyData() {

	IBodyFrame* pBodyFrame = nullptr;
	hResult = pBodyReader->AcquireLatestFrame(&pBodyFrame);
	if (SUCCEEDED(hResult)) {
		hResult = pBodyFrame->GetAndRefreshBodyData(BODY_COUNT, pBodyBuffer); // Get 6 Skeletons data
		if (SUCCEEDED(hResult)) extractBodyData();
		else updateSuccess = false;
	}
	else updateSuccess = false;
	SafeRelease(pBodyFrame);
}

void KinectV2::extractBodyData() {

	for (int count = 0; count < BODY_COUNT; count++) {
		BOOLEAN bTracked = false; // Body is Tracked or not
		hResult = pBodyBuffer[count]->get_IsTracked(&bTracked);
		if (SUCCEEDED(hResult) && bTracked) { // Do when Body is tracked
			Joint joint[JointType::JointType_Count]; // 25 Joint Data, where JointType_Count = 25
			hResult = pBodyBuffer[count]->GetJoints(JointType::JointType_Count, joint);
			if (SUCCEEDED(hResult)) { // Access joint data sucessfully

				struct AllBodyBean bb;
				bb.datetime = kdfm->getDateTimeStrg();
				for (int type = 0; type < JointType::JointType_Count; type++) {

					bb.joint[type] = joint[type];

					/*
					bb.pBodyOnCamera[type][0] = joint[type].Position.X;
					bb.pBodyOnCamera[type][1] = joint[type].Position.Y;
					bb.pBodyOnCamera[type][2] = joint[type].Position.Z;
					*/

					// Mapping CameraSpace to ColorSpace
					ColorSpacePoint colorSpacePoint = { 0 };
					hResult = pCoordinateMapper->MapCameraPointToColorSpace(joint[type].Position, &colorSpacePoint);
					if (SUCCEEDED(hResult)) {
						int x = static_cast<int>(colorSpacePoint.X);
						int y = static_cast<int>(colorSpacePoint.Y);
						bb.pBodyOnColor[type].x = x;
						bb.pBodyOnColor[type].y = y;
						//if ((x < 0) || (x > colorWidth) || (y < 0) || (y > colorHeight)); // Chect detected // *** here should be checked!! ***
					}
					else updateSuccess = false;

					/*
					// Mapping CameraSpace to DepthSpace
					DepthSpacePoint depthSpacePoint = { 0 };
					hResult = pCoordinateMapper->MapCameraPointToDepthSpace(joint[type].Position, &depthSpacePoint);
					if (SUCCEEDED(hResult)) {
						int d_x = static_cast<int>(depthSpacePoint.X);
						int d_y = static_cast<int>(depthSpacePoint.Y);
						if ((d_x >= 0) && (d_x < depthHeight) && (d_y >= 0) && (d_y < depthWidth))
							bb.pBodyOnDepth[type] = -1; //depthBufferMat.at<UINT16>(d_x, d_y);
						else bb.pBodyOnDepth[type] = 0; // out of screen
					}
					else updateSuccess = false;
					*/
				}
				//bodyInfo = bb;

				if (updateSuccess) {
					colorMatVector.push_back(colorBufferMat.clone());

					depthMatVector.push_back(depthBufferMatClone);
					depthMapPointsVector.push_back(depthSpacePoints);

					bodyInfoVector.push_back(bb);
				}
			}
			else updateSuccess = false;
		}
	}
}

void KinectV2::appendData() {

	if (updateSuccess) {
		colorMatVector.push_back(colorBufferMat.clone());

		depthMatVector.push_back(depthBufferMatClone);
		depthMapPointsVector.push_back(depthSpacePoints);

		bodyInfoVector.push_back(bodyInfo);
	}
}

Mat KinectV2::mappingDepthToColorSize(Mat depthBufferMat, vector<DepthSpacePoint> depthSpacePoints) {

	hResult = pCoordinateMapper->MapColorFrameToDepthSpace(depthBufferSize, (UINT16*)depthBufferMat.data, colorHeight*colorWidth, &depthSpacePoints[0]);

	if (SUCCEEDED(hResult)) {
		// Mapped Depth Buffer
		vector<UINT16> buffer(colorWidth*colorHeight);

		// Mapping Depth Data to Color Resolution
		for (int colorY = 0; colorY < colorHeight; colorY++) {
			for (int colorX = 0; colorX < colorWidth; colorX++) {
				const unsigned int colorIndex = colorY * colorWidth + colorX;
				const int depthX = static_cast<int>(depthSpacePoints[colorIndex].X + 0.5f);
				const int depthY = static_cast<int>(depthSpacePoints[colorIndex].Y + 0.5f);
				if ((0 <= depthX) && (depthX < depthWidth) && (0 <= depthY) && (depthY < depthHeight)) {
					const unsigned int depthIndex = depthY * depthWidth + depthX;
					buffer[colorIndex] = depthBufferMat.at<UINT16>(depthIndex);
				}
			}
		}

		Mat result = Mat(colorHeight, colorWidth, CV_16UC1, &buffer[0]).clone();
		//result.convertTo(result, CV_8UC1, 255.0f / 8000.0f, 0.0f); // nearest: 0 (black)
		//result.convertTo(result, CV_8UC1, -255.0f/8000.0f, 255.0f); // nearest: 255 (white)

		return result;
	}
	else cerr << "Failed depth to color registration!" << endl;
}

KinectV2::KinectV2(KinectDatasetFileManager *kdfm) {
	
	this->kdfm = kdfm;

	setUseOptimized(true); // Enable OpenCV CPU optimized code

	initKinect();
	cout << "Init Kinect v2 SUCCEESS!" << endl;

	setupBuffer();
	cout << "Init Buffer SUCCEESS!" << endl;
	cout << "Detection Work is Available Now!" << endl;
}

KinectV2::~KinectV2() {

	SafeRelease(pColorSource);
	SafeRelease(pColorReader);
	SafeRelease(pColorDescription);

	SafeRelease(pDepthSource);
	SafeRelease(pDepthReader);
	SafeRelease(pDepthDescription);

	SafeRelease(pBodySource);
	SafeRelease(pBodyReader);

	SafeRelease(pCoordinateMapper);

	if (pSensor) pSensor->Close();
	SafeRelease(pSensor);

	for (int count = 0; count < BODY_COUNT; count++) SafeRelease(pBodyBuffer[count]);
}

void KinectV2::update() {
		
	updateSuccess = true;

	readColorMat();
	readDepthMat();
	readBodyData();

	//appendData();
}

void KinectV2::save() {

	int limit = colorMatVector.size();
	cout << "there are " << limit << " data to save." << endl;
	for (int i = 0; i < limit-1; ++i) {
		stringstream strsid;
		strsid << setw(5) << setfill('0') << kdfm->getCurrentSampleID();
		string frameName = kdfm->getActivityName() + "_" + strsid.str() + ".jpg";
		string depthName = kdfm->getActivityName() + "_" + strsid.str() + ".csv";

		Mat colorMat = colorMatVector[i+1];
		kdfm->writeImage(kdfm->getColorDirPath(), frameName, colorMat);

		Mat depthMat = mappingDepthToColorSize(depthMatVector[i+1], depthMapPointsVector[i+1]);
		kdfm->writeMatCSV(depthName, depthMat);  // save real distance (depth in meter) in csv
		depthMat.convertTo(depthMat, CV_8UC1, 255.0f / 8000.0f, 0.0f); // nearest: 0 (black)
		//depthMat.convertTo(depthMat, CV_8UC1, -255.0f / 8000.0f, 255.0f); // nearest: 255 (white)
		kdfm->writeImage(kdfm->getDepthDirPath(), frameName, depthMat);

		Mat bodyMat = bodySave(kdfm, frameName, colorMat, bodyInfoVector[i]);
		kdfm->writeImage(kdfm->getBodyDirPath(), frameName, bodyMat);
	}
}