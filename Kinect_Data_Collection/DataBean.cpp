#include "stdafx.h"
#include "DataBean.h"

using namespace cv;
using namespace std;

// save body in csv file and draw it on color image and return the body image
Mat bodySave(KinectDatasetFileManager *kdfm, string frameName, Mat colorBufferMat, struct AllBodyBean bb) {

	Vec3b color[BODY_COUNT] = {
		Vec3b(255, 0, 0),
		Vec3b(0, 255, 0),
		Vec3b(0, 0, 255),
		Vec3b(255, 255, 0),
		Vec3b(255, 0, 255),
		Vec3b(0, 255, 255)
	};

	kdfm->writeCSV(frameName, bb.datetime, bb.colorWidth, bb.colorHeight);
	for (int i = 0; i < JointType::JointType_Count; ++i) {
		//kdfm->writeCSV(pBodyOnColor[count][i].x, pBodyOnColor[count][i].y);
		if (bb.pBodyOnColor[i].x == 0 and bb.pBodyOnColor[i].y == 0) kdfm->writeCSV("None", "None");
		else kdfm->writeCSV(bb.pBodyOnColor[i].x, bb.pBodyOnColor[i].y);
	}
	kdfm->writeEndl();

	//================================

	Mat outputMat = colorBufferMat.clone();

	for (int type = 0; type < JointType::JointType_Count; type++) {
		// draw skeleton on color image
		for (int type = 0; type < JointType::JointType_Count; type++)
			circle(outputMat, cv::Point(bb.pBodyOnColor[type].x, bb.pBodyOnColor[type].y), 10, static_cast< cv::Scalar >(color[0]), -1, CV_AA);

		drawSkeleton(outputMat, bb.pBodyOnColor, bb.joint, 0);
	}

	return outputMat;
}