#pragma once

#include <Kinect.h>
#include <opencv2/opencv.hpp>

#include "KinectDatasetFileManager.h"

using namespace cv;
using namespace std;

struct AllBodyBean {
	String datetime;
	int colorWidth = 1920;
	int colorHeight = 1080;

	Joint joint[JointType_Count];
	float pBodyOnCamera[JointType_Count][3] = { -1 };
	CvPoint pBodyOnColor[JointType_Count] = { cvPoint(-1,-1) };
	UINT16  pBodyOnDepth[JointType_Count] = { 0 };
};

Mat bodySave(KinectDatasetFileManager *, string, Mat, struct AllBodyBean);