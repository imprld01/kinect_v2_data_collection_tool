#include "stdafx.h"

using namespace std;
using namespace cv;

int main(int argc, char **argv) {

	string parentDirName = (argc == 1) ? "Activity" : argv[1];

	KinectDatasetFileManager* kdfm = new KinectDatasetFileManager("C:/Users/NOL/Desktop/KinectDataset", parentDirName,
																  "Color", "Depth", "DepthMat", "Skeleton", "Skeleton.csv");
	KinectV2* kinectv2 = new KinectV2(kdfm);
	
	cout << "Start Detection..." << endl;
	while (1) {
		kinectv2->update();
		waitKey(1);
		if (GetAsyncKeyState(VK_ESCAPE)) break;
	}
	cout << "Finish Detection!!" << endl;

	cout << "Start Saving..." << endl;
	kinectv2->save();
	cout << "Finish Saving!!" << endl;

	return 0;
}