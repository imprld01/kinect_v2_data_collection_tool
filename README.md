# Kinect_v2_Data_Collection_Tool

This tool can help collecting color, depth, pose data from Kinect v2 in C++.

### Development Environment

* C++
* Windows 10
* Visual Studio 2017 (v141)
* Windows SDK 10.0.16299.0
* Kinect for Windows SDK v2

### How To Use

1. Download and install **Visual Studio 2017** on Windows first
2. Then download and install **Kinect for Windows SDK v2**
3. Connect Kinect v2 on your computer with 3.0 USB port
4. Open and compile this project
5. Try to Run this project
6. If there is any error message, please check the following section for more info

### Error Message

* MSB8036 找不到Windows SDK版本10.0.16299.0, 安裝所需的Windows SDK版本, 或變更在專案屬性頁的SDK版本, 或在方案上按一下滑鼠右鍵, 並選取[重訂方案目標]
> 1. 使用VS 2017打開專案後
> 2. 在專案上右鍵選擇屬性
> 3. 依階層找到環境設定: 組態屬性/一般/Windows SDK 版本
> 4. 設定為較新或符合的版本 (ex. 10.0.17763.0)

### Development Setup Log

* 組態屬性設定步驟
> 1. 使用VS 2017打開專案後  
> 2. 在專案上右鍵選擇屬性  
> 3. 依階層找到環境設定: 組態屬性/偵錯/環境  
> 4. 設定為: path=.\lib\opencv\x64\vc14\bin

### License
[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)  
This webpage is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/) and the [MIT License](LICENSE).